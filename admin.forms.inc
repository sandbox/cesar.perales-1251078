<?php

/**
 * Edit Horoscope Form
 */
function horoscope_edit_form($form_state, $arg) {
    $horoscope = _get_horoscope_array($arg);
    $form['id'] = array(
        '#type' => 'hidden', 
        '#default_value' => $horoscope['id']
    );
    $form['name'] = array(
        '#type' => 'textfield',
        '#default_value' => $horoscope['name'],
        '#title' => t('Enter Horoscope Name'),
    );
    $form['enabled'] = array(
        '#type' => 'checkbox',
        '#default_value' => $horoscope['enabled'],
        '#title' => t('Enabled'),
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );
    return $form;
}

function horoscope_edit_form_submit($form_id, $form_values) {
    $exists = db_result(db_query('SELECT count(*) FROM {horoscope} WHERE id=%d', $form_values['values']['id']));
      if ($exists) {
        db_query('UPDATE {horoscope} SET name = "%s", enabled =%d WHERE id =%d', $form_values['values']['name'], $form_values['values']['enabled'],$form_values['values']['id']);
        	if(db_affected_rows()){
    			drupal_set_message(t('Updated !name: !enabled', array('!name' => $form_values['values']['name'], '!enabled' => $form_values['values']['enabled'])));
		  	}
      }
}
/**
 * Add Horoscope Form, edit name and availability
 */
function horoscope_add_form() {
    $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Enter Horoscope Name'),
    );
    $form['enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enabled'),
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );
    return $form;
}
function horoscope_add_form_submit($form_id, $form_values) {
	db_query('INSERT INTO {horoscope}(name,enabled) VALUES("%s", %d)', $form_values['values']['name'], $form_values['values']['enabled']);
	if(db_affected_rows()){
    	drupal_set_message(t('Insertado !name: !enabled', array('!name' => $form_values['values']['name'], '!enabled' => $form_values['values']['enabled'])));
    }
}

/**
 * Edit Horoscope Sign Form
 */
function horoscope_edit_sign_form($form_state, $arg) {
    $horoscope_sign = _get_horoscope_sign_array($arg);
    $form['id'] = array(
        '#type' => 'hidden', 
        '#default_value' => $horoscope_sign['id']
    );
    $form['horoscope_id'] = array(
        '#type' => 'hidden', 
        '#default_value' => $horoscope_sign['horoscope_id']
    );
    $form['name'] = array(
        '#type' => 'textfield',
        '#default_value' => $horoscope_sign['name'],
        '#title' => t('Enter Horoscope Name'),
    );
    $form['icon'] = array(
        '#type' => 'textfield',
        '#default_value' => $horoscope_sign['icon'],
        '#title' => t('Enter Horoscope Name'),
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );
    return $form;
}

function horoscope_edit_sign_form_submit($form_id, $form_values) {
    $exists = db_result(db_query('SELECT count(*) FROM {horoscope_signs} WHERE id=%d', $form_values['values']['id']));
      if ($exists) {
        db_query('UPDATE {horoscope_signs} SET name = "%s", icon="%s" WHERE id =%d and horoscope_id = %d', $form_values['values']['name'], $form_values['values']['icon'],$form_values['values']['id'],$form_values['values']['horoscope_id']);
        	if(db_affected_rows()){
    			drupal_set_message(t('Updated !name: !icon', array('!name' => $form_values['values']['name'], '!icon' => $form_values['values']['icon'])));
		  	}
      }
}
/**
 * Add Horoscope Sign Form
 */
function horoscope_add_sign_form($form_state, $arg) {
	$form['horoscope_id'] = array(
        '#type' => 'hidden', 
        '#default_value' => $arg,
    );
    $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Enter Horoscope Name'),
    );
    $form['icon'] = array(
        '#type' => 'textfield',
        '#title' => t('icon'),
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );
    return $form;
}

function horoscope_add_sign_form_submit($form_id, $form_values) {
	//db_query("INSERT INTO {horoscope_signs}(horoscope_id, name, icon) VALUES (1, 'Aries','".drupal_get_path('module', 'horoscope')."/img/zodiac/Aries.svg.png')");
	db_query('INSERT INTO {horoscope_signs}(horoscope_id,name,icon) VALUES(%d,"%s", "%s")', $form_values['values']['horoscope_id'], $form_values['values']['name'], $form_values['values']['icon']);
	if(db_affected_rows()){
    	drupal_set_message(t('Insertado !name: !icon', array('!name' => $form_values['values']['name'], '!icon' => $form_values['values']['icon'])));
    }
}