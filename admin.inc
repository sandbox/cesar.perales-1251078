<?php 

/**
 * Get Horoscope, returns array('id' => int, 'name' => string, 'enabled' => boolean)
 */
function _get_horoscope_array($horoscope_id) {
    $horoscope = db_fetch_array(db_query("SELECT * FROM {horoscope} WHERE id = %d",$horoscope_id));
    return $horoscope;
}

/**
 * Get Horoscope Sign, returns array('id' => int, 'horoscope_id' => int, 'name' => string, 'icon' => string)
 */
function _get_horoscope_sign_array($horoscope_id) {
    $horoscope = db_fetch_array(db_query("SELECT * FROM {horoscope_signs} WHERE id = %d",$horoscope_id));
    return $horoscope;
}
/**
 * List Horoscopes, Return table with all available horoscope names and links to edit and delete them
 */
function _horoscope_list (){
    $query = db_query("SELECT * FROM {horoscope}");
    $data = array();
    $i = 0;
    while ($row = db_fetch_array($query)) {
        $data[$i]['name'] = l($row['name'],'admin/settings/horoscope/'.$row['id']);
        $data[$i]['enabled'] = ($row['enabled'] == 1) ? t('enabled') : t('disabled');
		$data[$i]['actions'] = l(t('Add Sign'),'admin/settings/horoscope/'.$row['id'].'/add_sign');
        $data[$i]['actions'] .= '&nbsp;&nbsp;&nbsp;&nbsp;';
        $data[$i]['actions'] .= l(t('edit'),'admin/settings/horoscope/'.$row['id'].'/edit');
        $data[$i]['actions'] .= '&nbsp;&nbsp;&nbsp;&nbsp;';
        $data[$i]['actions'] .= l(t('delete'),'admin/settings/horoscope/'.$row['id'].'/delete');
        $i++;
    }
    $output = theme_table(array(t('name'), t('enabled'),t('actions')), $data);
    return $output;
}
/**
 * List Horoscopes Signs, Return table with all available Horoscope Signs 
 */
function _horoscope_list_signs ($arg){
    $query = db_query("SELECT * FROM {horoscope_signs} WHERE horoscope_id = %d",$arg);
    $data = array();
    $i = 0;
    while ($row = db_fetch_array($query)) {
        //$data[$i]['name'] = l($row['name'],'admin/settings/horoscope/'.$arg.'/'.$row['id']);
        $data[$i]['name'] = t($row['name']);
        $data[$i]['icon'] = $row['icon'];
        
        $data[$i]['actions'] = l(t('edit'),'admin/settings/horoscope/'.$arg.'/'.$row['id'].'/edit');
        $data[$i]['actions'] .= '&nbsp;&nbsp;&nbsp;&nbsp;';
        $data[$i]['actions'] .= l(t('delete'),'admin/settings/horoscope/'.$arg.'/'.$row['id'].'/delete');
        $i++;
    }
    $output = theme_table(array(t('name'), t('icon'),t('actions')), $data);
    return $output;
}